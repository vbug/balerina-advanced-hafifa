import Vue from 'vue'
import Vuex from 'vuex'
import messages from '@/store/modules/data/messages.js'
import threats from '@/store/modules/data/threats.js'
import realities from '@/store/modules/data/realities.js'
import launchingCountries from '@/store/modules/data/launchingCountries.js'
import tbmKind from '@/store/modules/data/tbmKind.js'
import messageRunner from '@/store/modules/form_inputs/messageRunner.js'
import threatForm from '@/store/modules/form_inputs/threatForm.js'

Vue.use(Vuex)

export default new Vuex.Store({

  modules: {
    // data tables:
    messages,
    threats,
    realities,
    launchingCountries,
    tbmKind,
    // forms:
    messageRunner,
    threatForm,
    },
})
