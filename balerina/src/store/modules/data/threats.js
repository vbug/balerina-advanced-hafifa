export default {
    namespaced: true,
    state: {
        threats: [
            {
                id: 1,
                reality: 2,
                lastUpdateTime: 'time',
                name: 'single message'
            },
            {
                id: 2,
                reality: 2,
                lastUpdateTime: 'time',
                name: 'single message'
            },
            {
                id: 3,
                reality: 2,
                lastUpdateTime: 'time',
                name: 'single message'
            },
            {
                id: 4,
                reality: 2,
                lastUpdateTime: 'time',
                name: 'single message'
            },
        ]
    },
    getters: {
        threats(state) {
            return state.threats;
        },
        threatById:(state)=>(threatID)=> {
            return state.threats.find(threat => threat == threatID)
        }
    },
    actions: {},
    mutations: {}
};