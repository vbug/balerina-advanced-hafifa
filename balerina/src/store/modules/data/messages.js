export default {
    namespaced: true,
    state: {
        messages: [
            {
                id: 1,
                threatId: 1,
                tbmKind: 'bla',
                delayFromStart: 0,
                timeUntilImpact: 10,
                launchX: 0,
                launchY: 0,
                impactX: 0,
                impactY: 0,
                elipseWidth: 0,
                elipseLength: 0,
                lastUpdateTime: 'ak',
                createTime: 'as',
                launchCountry: 'd'
            },
            {
                id: 2,
                threatId: 2,
                tbmKind: 'bla',
                delayFromStart: 0,
                timeUntilImpact: 10,
                launchX: 0,
                launchY: 0,
                impactX: 0,
                impactY: 0,
                elipseWidth: 0,
                elipseLength: 0,
                lastUpdateTime: 'ak',
                createTime: 'as',
                launchCountry: 'd'
            },
            {
                id: 3,
                threatId: 3,
                tbmKind: 'bla',
                delayFromStart: 0,
                timeUntilImpact: 10,
                launchX: 0,
                launchY: 0,
                impactX: 0,
                impactY: 0,
                elipseWidth: 0,
                elipseLength: 0,
                lastUpdateTime: 'ak',
                createTime: 'as',
                launchCountry: 'd'
            },
            {
                id: 4,
                threatId: 1,
                tbmKind: 'bla',
                delayFromStart: 0,
                timeUntilImpact: 10,
                launchX: 0,
                launchY: 0,
                impactX: 0,
                impactY: 0,
                elipseWidth: 0,
                elipseLength: 0,
                lastUpdateTime: 'ak',
                createTime: 'as',
                launchCountry: 'd'
            },
            {
                id: 5,
                threatId: 2,
                tbmKind: 'bla',
                delayFromStart: 0,
                timeUntilImpact: 10,
                launchX: 0,
                launchY: 0,
                impactX: 0,
                impactY: 0,
                elipseWidth: 0,
                elipseLength: 0,
                lastUpdateTime: 'ak',
                createTime: 'as',
                launchCountry: 'd'
            },
            {
                id: 6,
                threatId: 2,
                tbmKind: 'bla',
                delayFromStart: 0,
                timeUntilImpact: 10,
                launchX: 0,
                launchY: 0,
                impactX: 0,
                impactY: 0,
                elipseWidth: 0,
                elipseLength: 0,
                lastUpdateTime: 'ak',
                createTime: 'as',
                launchCountry: 'd'
            },
            {
                id: 7,
                threatId: 1,
                tbmKind: 'bla',
                delayFromStart: 0,
                timeUntilImpact: 10,
                launchX: 0,
                launchY: 0,
                impactX: 0,
                impactY: 0,
                elipseWidth: 0,
                elipseLength: 0,
                lastUpdateTime: 'ak',
                createTime: 'as',
                launchCountry: 'd'
            }
        ]
    },
    getters: {
        messages(state){
            return state.messages;
        },
        messagesByThreatId:(state)=>(threatId)=> {
            return state.messages.filter(message => message.threatId == threatId);
        }
    },
    actions: {},
    mutations: {}
};