export default {
    namespaced: true,
    state:{
        tbmKinds:[
            {
                id:1,
                name:'קונבנציונלי '
            },
            {
                id:2,
                name:'כימי'
            },
        ]
    },
    getters:{
        tbmKinds(state){
            return state.tbmKinds;
        },
        tbmKindsNames(state){
            return state.tbmKinds.map(tbmKind=>tbmKind.name);
        },
        tbmKindByName:(state)=>(tbmKindName)=>{
            return state.tbmKinds.filter(tbmKind=>tbmKind.name === tbmKindName)
        }
    },
    actions:{},
    mutations:{}
 };