export default {
    namespaced: true,
    state: {
        launchingCountries: [
            {
                id: 1,
                name: 'לבנון '
            },
            {
                id: 2,
                name: 'ירדן'
            },
        ]
    },
    getters: {
        launchingCountries(state) {
            return state.launchingCountries;
        },
        launchingCountryNames(state) {
            return state.launchingCountries.map(launchingCountry => launchingCountry.name);
        },
        launchingCountryByName: (state) => (launchingCountryName) => {
            return state.launchingCountries.filter(launchingCountry => launchingCountry.name === launchingCountryName)
        }
    },
    actions: {},
    mutations: {}
};