export default {
    namespaced: true,
    state:{
        realities:[
            {
                id:1,
                name:'מציאות אמת'
            },
            {
                id:2,
                name:'תרגילית'
            },
        ]
    },
    getters:{
        realities(state){
            return state.realities;
        },
        realityNames(state){
            return state.realities.map(reality=>reality.name);
        },
        realityByName:(state)=>(realityName)=>{
            return state.realities.filter(reality=>reality.name === realityName)
        }
    },
    actions:{},
    mutations:{}
 };