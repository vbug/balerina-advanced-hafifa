export default class {
    constructor() {
        this.state = {
        
            messageId: {
                name: "messageId",
                label: "מזהה מסר",
                value: "",
                rules:[
                  value=> value.length != 0 || "הזן מזהה מסר ",
                ]
                
              },
              messageLastUpdated: {
                name: "messageLastUpdated",
                label: "עודכן לאחרונה",
                value: ""
              },
              launchTime: {
                name: "launchTime",
                label: "זמן שיגור",
                value: ""
              },
              launchX: {
                name: "launchX",
                label: "נ.צ שיגור x",
                value: ""
              },
              launchY: {
                name: "launchY",
                label: "נ.צ שיגור y",
                value: ""
              },
        
              impactTime: {
                name: "impactTime",
                label: "זמן פגיעה",
                value: ""
              },
              impactX: {
                name: "impactX",
                label: "נ.צ פגיעה x",
                value: ""
              },
              impactY: {
                name: "impactY",
                label: "נ.צ פגיעה y",
                value: ""
              },
              elipseLength: {
                name: "elipseLength",
                label: "אורך אליפסה",
                value: ""
              },
              elipseWidth: {
                name: "elipseWidth",
                label: "רוחב אליפסה",
                value: ""
              },
        
              timeUntilImpact: {
                name: "timeUntilImpact",
                label: "זמן מעוף",
                value: ""
              },
              delayFromStart: {
                name: "delayFromStart",
                label: "השהייה",
                value: ""
              },
        
              threatId: {
                name: "threatId",
                label: "מזהה האיום",
                value: "intial"
              },
              threatName: {
                name: "threatName",
                label: "שם האיום",
                value: ""
              },
              lastUpdatedThreat: {
                name: "lastUpdatedThreat",
                label: "עודכן לאחרונה",
                value: ""
              },
              threatsPerSecond: {
                name: "threatsPerSecond",
                label: "מסרים לשניה",
                value: ""
              },
              threatDuration: {
                name: "threatDuration",
                label:  "משך האיום",
                value: ""
              },
        
              //v-autocompletes:
              receivingReallity: {
                name: "receivingReallity",
                label: "מציאות",
                value: "",
              },
              missileType: {
                name: "missileType",
                label: "סוג טיל",
                value: ""
              },
              launchingCountry: {
                name: "launchingCountry",
                label: "מדינה משגרת",
                value: ""
              },
              weaponType: {
                name: "weaponType",
                label: "סוג נשק",
                value: ""
              }
        },
            this.getters = {
              messageLastUpdated(state) {
                return state.messageLastUpdated;
            },
                messageId(state) {
                    return state.messageId;
                },
              
                launchTime(state) {
                    return state.launchTime;
                },
                launchX(state) {
                    return state.launchX;
                },
                launchY(state) {
                    return state.launchY;
                },
                impactTime(state) {
                    return state.impactTime;
                },
                impactX(state) {
                    return state.impactX;
                },
                impactY(state) {
                    return state.impactY;
                },
                elipseLength(state) {
                    return state.elipseLength;
                },
                elipseWidth(state) {
                    return state.elipseWidth;
                },
                timeUntilImpact(state) {
                    return state.timeUntilImpact;
                },
                delayFromStart(state) {
                    return state.delayFromStart;
                },
                threatId(state) {
                    return state.threatId;
                },
                threatName(state) {
                    return state.threatName;
                },
                lastUpdatedThreat(state) {
                    return state.lastUpdatedThreat;
                },
                threatsPerSecond(state) {
                    return state.threatsPerSecond;
                },
                threatDuration(state) {
                    return state.threatDuration;
                },
                receivingReallity(state) {
                    return state.receivingReallity;
                },
                missileType(state) {
                    return state.missileType;
                },
                launchingCountry(state) {
                    return state.launchingCountry;
                },
                weaponType(state) {
                    return state.weaponType;
                },
          
                
                
            },
            this.mutations = {
                updateValue(state, { input, newValue }) {
                    state[input.name].value = newValue;
                }
            },
            this.actions = {
                updateValue({commit}, { input, newValue}){
                    commit('updateValue', { input, newValue})
                },
               
            }

    }
}