import Inputs from '@/store/modules/form_inputs/inputs.js'
let inputs = new Inputs();


const state = {
   //...inputs.state,
   messageId: inputs.state.messageId,
   messageLastUpdated: inputs.state.messageLastUpdated,
   receivingReallity: inputs.state.receivingReallity,
   launchingCountry: inputs.state.launchingCountry,
   missileType: inputs.state.missileType,
   launchTime: inputs.state.launchTime,
   launchX: inputs.state.launchX,
   launchY: inputs.state.launchY,
   impactTime: inputs.state.impactTime,
   impactX: inputs.state.impactX,
   impactY: inputs.state.impactY,
   elipseLength: inputs.state.elipseLength,
   elipseWidth: inputs.state.elipseWidth,



};
const getters = {
   //...inputs.getters,
   messageId: inputs.getters.messageId,
   messageLastUpdated: inputs.getters.messageLastUpdated,
   receivingReallity: inputs.getters.receivingReallity,
   launchingCountry: inputs.getters.launchingCountry,
   missileType: inputs.getters.missileType,
   launchTime: inputs.getters.launchTime,
   launchX: inputs.getters.launchX,
   launchY: inputs.getters.launchY,
   impactTime: inputs.getters.impactTime,
   impactX: inputs.getters.impactX,
   impactY: inputs.getters.impactY,
   elipseLength: inputs.getters.elipseLength,
   elipseWidth: inputs.getters.elipseWidth,
 
  
};
const actions = {
   ...inputs.actions,
   createMessage({state}) {
      let message = {}
      for (let key in state) {
         message[key] = state[key].value
      }
      return message;
      //add logic for inserting into db
   }

};
const mutations = {
   ...inputs.mutations,
};

export default {
   namespaced: true,
   state,
   getters,
   actions,
   mutations
};