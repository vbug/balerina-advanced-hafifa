import Inputs from '@/store/modules/form_inputs/inputs.js'
let inputs = new Inputs();


const state = {
   ...inputs.state,


};
const getters = {
   ...inputs.getters,
};
const actions = {
   ...inputs.actions,
   createMessage({state}) {
      let message = {}
      for (let key in state) {
         message[key] = state[key].value
      }
      return message;
      //add logic for inserting into db
   }

};
const mutations = {
   ...inputs.mutations,
};

export default {
   namespaced: true,
   state,
   getters,
   actions,
   mutations
};