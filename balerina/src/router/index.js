import Vue from 'vue'
import VueRouter from 'vue-router'
import Shell from '@/models/Shell.vue'
import MessageRunner from '@/models/MessageRunner.vue'
import Threats from '@/models/Threats.vue'
import EditThreat from '@/models/EditThreat.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    component: Shell,
    children:[
      {
        path: 'messageRunner',
        name: 'messageRunner',
        component: MessageRunner,
        meta:{
          isExpanded: false
        }
      },
      {
        path: 'threats',
        name: 'threats',
        component: Threats,
        meta:{
          isExpanded: true
        }
      },
      {
        path: 'editThreat/:id',
        name: 'editThreat',
        component: EditThreat,
        meta:{
          isExpanded: false
        }
      }
    ]
  },

]

const router = new VueRouter({
  routes
})

export default router
